package com.liveedu.currencyconverter;

import com.liveedu.currencyconverter.models.Currency;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource("classpath:application.properties")
public class IntegrationTests {

    private RestTemplate restTemplate;

    private String baseUrl = "http://localhost:8080";

    @Before
    public void init() {
        restTemplate = new RestTemplate();
    }

    @Test
    public void getAllCurrenciesShouldReturn200OK() {
        ResponseEntity<List<Currency>> rateResponse =
                restTemplate.exchange(baseUrl + "/currencies",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Currency>>() {
                        });
        List<Currency> rates = rateResponse.getBody();

        Assert.assertFalse("Rates should not be empty",rates.isEmpty());

    }
}
