package com.liveedu.currencyconverter;

import com.liveedu.currencyconverter.models.ConvertionData;
import com.liveedu.currencyconverter.models.Currency;
import com.liveedu.currencyconverter.repositories.CurrencyRepository;
import com.liveedu.currencyconverter.services.CurrencyConversionService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyConverterUnitTests {

	@Autowired
	CurrencyConversionService subject;

	@Mock
	CurrencyRepository currencyRepository;

	@Before
	public void setup() {
		subject = new CurrencyConversionService(currencyRepository);
	}

	@Test
	public void convertBetweenTwoCurrenciesShouldBeSuccessful() {
		Currency currencyUSD = new Currency("USD", 0.8);
		Currency currencyEUR = new Currency("EUR", 1);

		Mockito.when(currencyRepository.findById("USD"))
				.thenReturn(Optional.of(currencyUSD));

		Mockito.when(currencyRepository.findById("EUR"))
				.thenReturn(Optional.of(currencyEUR));

		ConvertionData convertionData = new ConvertionData("USD", "EUR", 10d);

		Optional<Double> result = subject.convert(convertionData);
		Assert.assertTrue("Result should not be empty", result.isPresent());
		Assert.assertEquals(12.5, result.get().doubleValue(), 0);
	}


	@Test
	public void convertBetweenTwoCurrenciesShouldReturnEmpty() {
		Mockito.when(currencyRepository.findById(Mockito.anyString()))
				.thenReturn(Optional.empty());


		// OUI and WEA are currencies that do not exist
		ConvertionData convertionData = new ConvertionData("WEA", "OIU", 10d);

		Optional<Double> result = subject.convert(convertionData);
		Assert.assertTrue("Result should be empty", !result.isPresent());
	}


	@Test
	public void getAllCurrenciesShouldReturnAPopulatedAndOrderedList(){

		List<Currency> currencyList = new ArrayList<>();

		Currency currencyZMW = new Currency("ZMW", 1);
		Currency currencyEUR = new Currency("EUR", 1);
		Currency currencyAED = new Currency("AED", 4.2);
		Currency currencyUSD = new Currency("USD", 0.8);

		currencyList.add(currencyZMW);
		currencyList.add(currencyEUR);
		currencyList.add(currencyAED);
		currencyList.add(currencyUSD);


		Mockito.when(currencyRepository.findAll())
				.thenReturn(currencyList);

		List<Currency> result = subject.getAllCurrencies();
		Assert.assertFalse("Result should not be empty", result.isEmpty());
		Assert.assertEquals(result.get(0).getName(), "AED");
		Assert.assertEquals(result.get(1).getName(), "EUR");
		Assert.assertEquals(result.get(2).getName(), "USD");
		Assert.assertEquals(result.get(3).getName(), "ZMW");
	}

}
