package com.liveedu.currencyconverter.services;

import com.liveedu.currencyconverter.models.ConvertionData;
import com.liveedu.currencyconverter.models.Currency;
import com.liveedu.currencyconverter.repositories.CurrencyRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class CurrencyConversionService {

    private CurrencyRepository currencyRepository;

    public CurrencyConversionService(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    public Optional<Double> convert(ConvertionData convertionData) {
        String from = convertionData.getFrom().toUpperCase();
        String to = convertionData.getTo().toUpperCase();
        Optional<Currency> fromOptional = this.currencyRepository.findById(from);
        Optional<Currency> toOptional = this.currencyRepository.findById(to);

        if(fromOptional.isPresent() && toOptional.isPresent()) {

            Double value = convertionData.getValue() * (toOptional.get().getValueInUsd() / fromOptional.get().getValueInUsd());
            return Optional.of(new BigDecimal(value.toString()).setScale(2,RoundingMode.HALF_UP).doubleValue());
        }

        return Optional.empty();
    }

    public List<Currency> getAllCurrencies() {
        List<Currency> currencies = this.currencyRepository.findAll();
        currencies.sort(Comparator.comparing(Currency::getName));
        return currencies;
    }

}
