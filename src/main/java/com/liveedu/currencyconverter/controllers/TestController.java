package com.liveedu.currencyconverter.controllers;

import com.liveedu.currencyconverter.models.ConvertionData;
import com.liveedu.currencyconverter.models.Currency;
import com.liveedu.currencyconverter.services.CurrencyConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class TestController {

    CurrencyConversionService currencyConversionService;

    public TestController(CurrencyConversionService currencyConversionService) {
       this.currencyConversionService = currencyConversionService;
    }

    @RequestMapping(value = "/convert-currency", produces = { "application/json" }, method = RequestMethod.POST)
    public ResponseEntity<Double> convertCurrency(@RequestBody ConvertionData convertionData) {
        Optional<Double> conversion = currencyConversionService.convert(convertionData);
        if(!conversion.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(conversion.get(), HttpStatus.OK);


    }

    @RequestMapping(value = "/currencies", produces = { "application/json" }, method = RequestMethod.GET)
    public ResponseEntity<List<Currency>> convertCurrency() {
        return new ResponseEntity<>(this.currencyConversionService.getAllCurrencies(), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hello() {
        return "Tutorial for Liveedu!";
    }
}
