FROM frolvlad/alpine-oraclejdk8:slim

MAINTAINER Ricardo Neves

ENV SERVICE_NAME currency-converter

RUN mkdir -p /usr/opt/service

COPY target/$SERVICE_NAME*.jar /usr/opt/service/$SERVICE_NAME.jar

EXPOSE 8080:8080

CMD java -jar /usr/opt/service/$SERVICE_NAME.jar